const express = require('express');
const router = express.Router();
const searchController = require('../../controllers/search/search');

router.get('/get', searchController.getData);
router.post('/find', searchController.findData);

module.exports = router;
